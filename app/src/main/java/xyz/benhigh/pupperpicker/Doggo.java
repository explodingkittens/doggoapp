package xyz.benhigh.pupperpicker;

public class Doggo {
    private String url;
    private String breed;

    public Doggo(String url, String breed) {
        this.url = url;
        this.breed = breed;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}
